[![Build Status](https://img.shields.io/travis/marcelovicentegc/Restaurant-model.svg?branch=master&style=flat-square)](https://travis-ci.org/marcelovicentegc/Restaurant-model)
[![Pipeline](https://gitlab.com/marcelovicentegc/Restaurant-model/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/marcelovicentegc/Restaurant-model)
[![CircleCI](https://circleci.com/gh/marcelovicentegc/Restaurant-model/tree/master.svg?style=shield)](https://circleci.com/gh/marcelovicentegc/Restaurant-model/tree/master)

# 🍔 Restaurant-model

<!-- ## Demo -->

## Directions

1. Clone this repo: `git clone https://github.com/marcelovicentegc/Restaurant-model.git`
2. Change directory into cloned repo: `cd Restaurant-model`
3. Install dependencies: `yarn install` or `npm install`
4. Create a Postgres database and set your credentials on a `ormconfig.json` file, similar to `ormconfig.example.json`
5. You are good to go. Run `yarn start` or `npm start`
6. To login, use the email: `admin@example.com`, and password: `admin`
