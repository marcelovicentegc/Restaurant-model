import * as bcrypt from "bcrypt";
import { createWriteStream } from "fs";
import { IResolvers } from "graphql-tools";
import * as shortid from "shortid";
import { getConnection } from "typeorm";
import {
  About,
  Beer,
  Burger,
  Product,
  Snack,
  User
} from "../database/entities/index";

export const storeUpload = async ({
  createReadStream,
  filename
}: any): Promise<any> => {
  const id = shortid.generate();
  new Promise((resolve, reject) =>
    createReadStream()
      .pipe(createWriteStream("dist/" + id + "-" + filename))
      .on("finish", () => resolve({ id }))
      .on("error", reject)
  );

  const stream = "./" + id + "-" + filename;
  return stream;
};

const resolvers: IResolvers = {
  Query: {
    product: (_, { id }) => Product.findOne(id),
    products: () => Product.find(),

    about: (_, { id }) => About.findOne(id),
    burger: (_, { id }) => Burger.findOne(id),
    snack: (_, { id }) => Snack.findOne(id),
    beer: (_, { id }) => Beer.findOne(id),
    user: (_, __, { req }) => {
      if (!req.session.userId) {
        console.log(req.session);
        return null;
      }

      return User.findOne(req.session.userId);
    },

    abouts: () => About.find(),
    burgers: () => Burger.find(),
    snacks: () => Snack.find(),
    beers: () => Beer.find(),
    users: () => User.find()
  },
  Mutation: {
    createProduct: async (_, args) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      const product = Product.create({
        title: args.title,
        fileUrl: stream,
        description: args.description,
        price: args.price,
        location: args.location
      });
      await product.save();
      return product;
    },
    updateProduct: async (_, { id, ...args }) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      try {
        await Product.update(id, {
          title: args.title,
          fileUrl: stream,
          description: args.description,
          price: args.price,
          location: args.location
        });
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },
    deleteProduct: async (_, { id }) => {
      try {
        const deleteQuery = getConnection()
          .createQueryBuilder()
          .delete()
          .from(Product)
          .where("id = :id", { id: id });
        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },

    createUser: async (_, { fullName, email, password }) => {
      const hashedPassword = await bcrypt.hash(password, 12);
      const user = User.create({
        fullName: fullName,
        email: email,
        password: hashedPassword
      });
      await user.save();
      return user;
    },
    updateUser: async (_, { id, ...args }) => {
      try {
        await User.update(id, args);
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },
    deleteUser: async (_, { id }) => {
      try {
        await User.remove(id);
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },
    loginUser: async (_, { email, password }, { req }) => {
      const user = await User.findOne({
        where: { email }
      });
      if (!user) {
        throw new Error("There is no user with that email.");
      }

      const valid = await bcrypt.compare(password, user.password);
      if (!valid) {
        throw new Error("Incorrect password.");
      }
      req.session.userId = user.id;
      return user;
    },
    logoutUser: async (_, __, { req, res }) => {
      await new Promise(res => req.session.destroy(() => res()));
      res.clearCookie("connect.sid");
      return true;
    },

    createAbout: async (_, args) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      const about = About.create({
        title: args.title,
        fileUrl: stream,
        description: args.description,
        location: args.location
      });
      await about.save();
      return about;
    },
    updateAbout: async (_, { id, ...args }) => {
      if (args.file !== undefined) {
        const { createReadStream, filename } = await args.file;
        const stream = await storeUpload({ createReadStream, filename });
        try {
          await About.update(id, {
            title: args.title,
            fileUrl: stream,
            description: args.description,
            location: args.location
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      } else {
        try {
          await About.update(id, {
            title: args.title,
            description: args.description,
            location: args.location
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      }
    },
    deleteAbout: async (_, { id }) => {
      try {
        const deleteQuery = getConnection()
          .createQueryBuilder()
          .delete()
          .from(About)
          .where("id = :id", { id: id });
        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },

    createBurger: async (_, args) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      const burger = await Burger.create({
        title: args.title,
        fileUrl: stream,
        description: args.description,
        price: args.price
      });
      await burger.save();
      return burger;
    },
    updateBurger: async (_, { id, ...args }) => {
      if (args.file !== undefined) {
        const { createReadStream, filename } = await args.file;
        const stream = await storeUpload({ createReadStream, filename });
        try {
          await Burger.update(id, {
            title: args.title,
            fileUrl: stream,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      } else {
        try {
          await Burger.update(id, {
            title: args.title,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      }
    },
    deleteBurger: async (_, { id }) => {
      try {
        const deleteQuery = getConnection()
          .createQueryBuilder()
          .delete()
          .from(Burger)
          .where("id = :id", { id: id });
        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },

    createBeer: async (_, args) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      const beer = Beer.create({
        title: args.title,
        fileUrl: stream,
        description: args.description,
        price: args.price
      });
      await beer.save();
      console.log(beer);
      return beer;
    },
    updateBeer: async (_, { id, ...args }) => {
      if (args.file !== undefined) {
        const { createReadStream, filename } = await args.file;
        const stream = await storeUpload({ createReadStream, filename });
        try {
          await Beer.update(id, {
            title: args.title,
            fileUrl: stream,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      } else {
        try {
          await Beer.update(id, {
            title: args.title,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      }
    },
    deleteBeer: async (_, { id }) => {
      try {
        const deleteQuery = getConnection()
          .createQueryBuilder()
          .delete()
          .from(Beer)
          .where("id = :id", { id: id });
        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    },

    createSnack: async (_, args) => {
      const { createReadStream, filename } = await args.file;
      const stream = await storeUpload({ createReadStream, filename });
      const snack = Snack.create({
        title: args.title,
        fileUrl: stream,
        description: args.description,
        price: args.price
      });
      await snack.save();
      return snack;
    },
    updateSnack: async (_, { id, ...args }) => {
      if (args.file !== undefined) {
        const { createReadStream, filename } = await args.file;
        const stream = await storeUpload({ createReadStream, filename });
        try {
          await Snack.update(id, {
            title: args.title,
            fileUrl: stream,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      } else {
        try {
          await Snack.update(id, {
            title: args.title,
            description: args.description,
            price: args.price
          });
        } catch (err) {
          console.log(err);
          return false;
        }
        return true;
      }
    },
    deleteSnack: async (_, { id }) => {
      try {
        const deleteQuery = getConnection()
          .createQueryBuilder()
          .delete()
          .from(Snack)
          .where("id = :id", { id: id });
        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }
      return true;
    }
  }
};

export default resolvers;
