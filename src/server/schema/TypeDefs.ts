import gql from "graphql-tag";

export const typeDefs = gql`
  scalar Upload

  type Product {
    id: ID!
    type: String!
    file: Upload!
    fileUrl: String
    title: String!
    description: String!
    price: Float
    location: String
  }

  type Burger {
    id: ID!
    file: Upload!
    fileUrl: String
    title: String!
    description: String!
    price: Float!
  }
  type Snack {
    id: ID!
    file: Upload!
    fileUrl: String
    title: String!
    description: String!
    price: Float!
  }
  type Beer {
    id: ID!
    file: Upload!
    fileUrl: String
    title: String!
    description: String!
    price: Float!
  }
  type About {
    id: ID!
    file: Upload!
    fileUrl: String
    title: String!
    description: String!
    location: String!
  }

  type User {
    id: ID!
    fullName: String!
    email: String!
    password: String!
  }

  type Query {
    product(id: ID!): Product!
    products: [Product!]!

    about(id: ID!): About!
    burger(id: ID!): Burger!
    snack(id: ID!): Snack!
    beer(id: ID!): Beer!
    user: User!

    abouts: [About!]!
    burgers: [Burger!]!
    snacks: [Snack!]!
    beers: [Beer!]!
    users: [User!]!
  }

  type Mutation {
    createProduct(
      type: String!
      file: Upload
      fileUrl: String
      description: String!
      price: Float
      location: String
    ): Product!
    updateProduct(
      id: ID!
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      price: Float
      location: String
    ): Boolean!
    deleteProduct(id: ID!): Boolean!

    createUser(fullName: String!, email: String!, password: String!): User!
    updateUser(
      id: ID!
      fullName: String!
      email: String!
      password: String!
    ): Boolean!
    deleteUser(id: ID!): Boolean!
    loginUser(email: String!, password: String!): User!
    logoutUser: Boolean!

    createAbout(
      title: String!
      file: Upload
      fileUrl: String
      description: String!
      location: String!
    ): About!
    updateAbout(
      id: ID!
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      location: String!
    ): Boolean!
    deleteAbout(id: ID!): Boolean!

    createBurger(
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      price: Float!
    ): Burger!
    updateBurger(
      id: ID!
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      price: Float!
    ): Boolean!
    deleteBurger(id: ID!): Boolean!

    createSnack(
      title: String!
      file: Upload
      fileUrl: String
      description: String!
      price: Float!
    ): Snack!
    updateSnack(
      id: ID!
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      price: Float!
    ): Boolean!
    deleteSnack(id: ID!): Boolean!

    createBeer(
      title: String!
      file: Upload
      fileUrl: String
      description: String!
      price: Float!
    ): Beer!
    updateBeer(
      id: ID!
      file: Upload
      fileUrl: String
      title: String!
      description: String!
      price: Float!
    ): Boolean!
    deleteBeer(id: ID!): Boolean!
  }
`;

export default typeDefs;
