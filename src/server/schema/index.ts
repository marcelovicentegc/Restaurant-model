import mainSchema from "./Schema";
import { mergeSchemas } from "graphql-tools";

export const schema = mergeSchemas({
  schemas: [mainSchema]
});
