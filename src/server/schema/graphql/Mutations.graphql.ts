import gql from "graphql-tag";

export const createUser = gql`
  mutation CREATE_USER(
    $fullName: String!
    $email: String!
    $password: String!
  ) {
    createUser(fullName: $fullName, email: $email, password: $password) {
      fullName
      email
      password
    } # Returns boolean
  }
`;
export const loginUser = gql`
  mutation LOGIN_USER($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      id
      email
    }
  }
`;
export const logoutUser = gql`
  mutation LOGOUT_USER {
    logoutUser
  }
`;

export const createAbout = gql`
  mutation CREATE_ABOUT(
    $title: String!
    $file: Upload
    $fileUrl: String
    $description: String!
    $location: String!
  ) {
    createAbout(
      title: $title
      file: $file
      fileUrl: $fileUrl
      description: $description
      location: $location
    ) {
      title
      fileUrl
      description
      location
    }
  }
`;
export const updateAbout = gql`
  mutation UPDATE_ABOUT(
    $id: ID!
    $file: Upload
    $title: String!
    $description: String!
    $location: String!
  ) {
    updateAbout(
      id: $id
      file: $file
      title: $title
      description: $description
      location: $location
    )
  }
`;
export const deleteAbout = gql`
  mutation DELETE_ABOUT($id: ID!) {
    deleteAbout(id: $id)
  }
`;

export const createBurger = gql`
  mutation CREATE_BURGER(
    $file: Upload
    $fileUrl: String
    $title: String!
    $description: String!
    $price: Float!
  ) {
    createBurger(
      file: $file
      fileUrl: $fileUrl
      title: $title
      description: $description
      price: $price
    ) {
      fileUrl
      title
      description
      price
    }
  }
`;
export const updateBurger = gql`
  mutation UPDATE_BURGER(
    $id: ID!
    $file: Upload
    $title: String!
    $description: String!
    $price: Float!
  ) {
    updateBurger(
      id: $id
      file: $file
      title: $title
      description: $description
      price: $price
    )
  }
`;
export const deleteBurger = gql`
  mutation DELETE_BURGER($id: ID!) {
    deleteBurger(id: $id)
  }
`;

export const createSnack = gql`
  mutation CREATE_SNACK(
    $title: String!
    $file: Upload
    $fileUrl: String
    $description: String!
    $price: Float!
  ) {
    createSnack(
      title: $title
      file: $file
      fileUrl: $fileUrl
      description: $description
      price: $price
    ) {
      fileUrl
      title
      description
      price
    }
  }
`;
export const updateSnack = gql`
  mutation UPDATE_SNACK(
    $id: ID!
    $file: Upload
    $title: String!
    $description: String!
    $price: Float!
  ) {
    updateSnack(
      id: $id
      file: $file
      title: $title
      description: $description
      price: $price
    )
  }
`;
export const deleteSnack = gql`
  mutation DELETE_SNACK($id: ID!) {
    deleteSnack(id: $id)
  }
`;

export const createBeer = gql`
  mutation CREATE_BEER(
    $title: String!
    $file: Upload
    $fileUrl: String
    $description: String!
    $price: Float!
  ) {
    createBeer(
      title: $title
      file: $file
      fileUrl: $fileUrl
      description: $description
      price: $price
    ) {
      fileUrl
      title
      description
      price
    }
  }
`;
export const updateBeer = gql`
  mutation UPDATE_BEER(
    $id: ID!
    $file: Upload
    $title: String!
    $description: String!
    $price: Float!
  ) {
    updateBeer(
      id: $id
      file: $file
      title: $title
      description: $description
      price: $price
    )
  }
`;
export const deleteBeer = gql`
  mutation DELETE_BEER($id: ID!) {
    deleteBeer(id: $id)
  }
`;
