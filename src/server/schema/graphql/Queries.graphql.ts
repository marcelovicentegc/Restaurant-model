import gql from "graphql-tag";

export const getUser = gql`
  query GET_USER {
    user {
      id
      email
      fullName
    }
  }
`;

export const getAbouts = gql`
  query GET_ABOUTS {
    abouts {
      id
      fileUrl
      title
      description
      location
    }
  }
`;

export const getAbout = gql`
  query GET_ABOUT($id: ID!) {
    about(id: $id) {
      id
      fileUrl
      title
      description
      location
    }
  }
`;

export const getBurgers = gql`
  query GET_BURGERS {
    burgers {
      id
      fileUrl
      title
      description
      price
    }
  }
`;

export const getBurger = gql`
  query GET_BURGER($id: ID!) {
    burger(id: $id) {
      id
      fileUrl
      title
      description
      price
    }
  }
`;

export const getSnacks = gql`
  query GET_SNACKS {
    snacks {
      id
      fileUrl
      title
      description
      price
    }
  }
`;

export const getSnack = gql`
  query GET_SNACK($id: ID!) {
    snack(id: $id) {
      id
      fileUrl
      title
      description
      price
    }
  }
`;

export const getBeers = gql`
  query GET_BEERS {
    beers {
      id
      fileUrl
      title
      description
      price
    }
  }
`;

export const getBeer = gql`
  query GET_BEER($id: ID!) {
    beer(id: $id) {
      id
      fileUrl
      title
      description
      price
    }
  }
`;
