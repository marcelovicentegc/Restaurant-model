import resolvers from "./Resolvers";
import typeDefs from "./TypeDefs";
import { makeExecutableSchema } from "graphql-tools";

const mainSchema = makeExecutableSchema({
  typeDefs: typeDefs,
  resolvers: resolvers
});

export default mainSchema;
