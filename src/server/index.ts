import { ApolloServer } from "apollo-server-express";
import * as bcrypt from "bcrypt";
import * as bodyParser from "body-parser";
import * as express from "express";
import * as session from "express-session";
import { createReadStream, createWriteStream } from "fs";
import "reflect-metadata";
import { createConnection } from "typeorm";
import { About, Beer, Burger, Snack, User } from "./database/entities";
import { schema } from "./schema/index";

const startServer = async () => {
  const server = new ApolloServer({
    schema,
    playground: {
      endpoint: "/api/playground"
    },
    context: ({ req, res }: any) => ({ req, res })
  });

  let retries = 5;
  while (retries) {
    try {
      await createConnection().then(connection => {
        console.log("\x1b[36m%s\x1b[0m", "Connected to database");
      });

      if (process.env.NODE_ENV === "development") {
        const hashedPassword = await bcrypt.hash("admin", 12);
        const defaultUser = User.create({
          fullName: "Pierre",
          email: "admin@example.com",
          password: hashedPassword
        });
        await defaultUser.save();
        console.log("\x1b[36m%s\x1b[0m", "Created default user");

        const saveUpload = (readStream: string, filename: string) => {
          new Promise((resolve, reject) =>
            createReadStream(readStream)
              .pipe(createWriteStream("dist/" + filename))
              .on("finish", () => resolve())
              .on("error", reject)
          );
          let stream = "./" + filename;
          return stream;
        };
        const aboutStream = await saveUpload(
          "src/server/__tests__/assets/house_building.png",
          "house_building.png"
        );
        const about = await About.create({
          title: "Who we are",
          fileUrl: aboutStream,
          description: "We love serving food since 2019",
          location: "Bruxels"
        });
        await about.save();

        const burgerStream = await saveUpload(
          "src/server/__tests__/assets/hamburger.png",
          "hamburger.png"
        );
        const burger = await Burger.create({
          title: "Super burger",
          fileUrl: burgerStream,
          description: "It is so good it will get you addicted to it",
          price: 30
        });
        await burger.save();

        const snackStream = await saveUpload(
          "src/server/__tests__/assets/hot_pepper.png",
          "hot_pepper.png"
        );
        const snack = await Snack.create({
          title: "Crunchy peppers",
          fileUrl: snackStream,
          description: "You will sweat your eyes out with this",
          price: 12
        });
        await snack.save();

        const beerStream = saveUpload(
          "src/server/__tests__/assets/beer_mug.png",
          "beer_mug.png"
        );
        const beer = await Beer.create({
          title: "Venon beer",
          fileUrl: beerStream,
          description:
            "A massive 65% ABV will get you as high as it tastes: peat smoked malt (whatever that tastes like)",
          price: 12
        });
        await beer.save();
        console.log("\x1b[36m%s\x1b[0m", "Populated database");
      }

      break;
    } catch (err) {
      console.log(err);
      retries -= 1;
      console.log(`${retries} retries left`);
      await new Promise(res => setTimeout(res, 5000));
    }
  }

  const app = express();
  app.use(
    session({
      secret: "9314i192481290",
      resave: false,
      saveUninitialized: false
    })
  );
  app.use(bodyParser.json());
  server.applyMiddleware({
    app,
    path: "/api/playground",
    cors: {
      origin: ["http://localhost:3000", "http://127.0.0.1:3000"],
      credentials: true
    }
  });

  app.listen(8080, () => {
    console.log("\x1b[34m", "Server is ready for requests on port 8080 ");
  });
};

startServer();
