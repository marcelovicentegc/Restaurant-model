import * as faker from "faker";
import { Response } from "supertest";
const url = `http://127.0.0.1:8080/api/playground`;
const request = require("supertest")(url);

describe(" USER TESTS ------------------------------------------", () => {
  test("Creates an user", async () => {
    try {
      await request
        .post("/")
        .send({
          query: `mutation { createUser(fullName: "${faker.name.findName()}", email: "${faker.internet.email()}", password: "${faker.internet.password()}") { fullName id password } }`
        })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const users = res.body.data;
          console.log("Got: " + JSON.stringify(users));
        });
    } catch (err) {
      console.log(err);
    }
  });
});
