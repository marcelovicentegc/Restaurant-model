import { fetch } from "apollo-env";
import * as faker from "faker";
import * as FormData from "form-data";
import { createReadStream } from "fs";
import { Response } from "graphql-upload";
import * as supertest from "supertest";
const url = `http://127.0.0.1:8080/api/playground`;
const request = supertest(url);

describe(" SNACK TESTS ------------------------------------------", () => {
  test("Creates a snack", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const price = parseFloat(faker.commerce.price());

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
          $fileUrl: String
          $title: String!
          $description: String!
          $price: Float!) { createSnack(title: $title, description: $description, price: $price, file: $file, fileUrl: $fileUrl) { title description price fileUrl } }`,
        variables: {
          title: title,
          description: description,
          price: price,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/hot_pepper.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });

  test("Returns all snacks", async () => {
    try {
      await request
        .post("/")
        .send({ query: "{ snacks { id title description price fileUrl } }" })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const snacks = res.body.data;
          console.log("Got: " + JSON.stringify(snacks));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Returns a single snack", async () => {
    try {
      await request
        .post("/")
        .send({ query: "{ snack(id:1) { title description price fileUrl } }" })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const snack = res.body.data;
          console.log("Got: " + JSON.stringify(snack));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Updates every property of a snack", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const price = parseFloat(faker.commerce.price());

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
            $fileUrl: String
            $title: String!
            $description: String!
            $price: Float!) { updateSnack(id:1 title: $title, description: $description, price: $price, file: $file, fileUrl: $fileUrl) }`,
        variables: {
          title: title,
          description: description,
          price: price,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/hot_pepper.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });
});
