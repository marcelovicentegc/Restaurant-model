import { fetch } from "apollo-env";
import * as faker from "faker";
import * as FormData from "form-data";
import { createReadStream } from "fs";
import { Response } from "graphql-upload";
import * as supertest from "supertest";
const url = `http://127.0.0.1:8080/api/playground`;
const request = supertest(url);

describe(" ABOUT TESTS ------------------------------------------", () => {
  test("Creates an about", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const location = faker.address.city();

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
          $fileUrl: String
          $title: String!
          $description: String!
          $location: String!) { createAbout(title: $title, description: $description, location: $location, file: $file, fileUrl: $fileUrl) { title description location fileUrl } }`,
        variables: {
          title: title,
          description: description,
          location: location,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/house_building.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });

  test("Returns all abouts", async () => {
    try {
      await request
        .post("/")
        .send({ query: "{ abouts { id title description location fileUrl } }" })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const abouts = res.body.data;
          console.log("Got: " + JSON.stringify(abouts));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Returns a single about", async () => {
    try {
      await request
        .post("/")
        .send({
          query: "{ about(id:1) { title description location fileUrl } }"
        })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const about = res.body.data;
          console.log("Got: " + JSON.stringify(about));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Updates every property of an about", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const location = faker.address.city();

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
            $fileUrl: String
            $title: String!
            $description: String!
            $location: String!) { updateAbout(id:1 title: $title, description: $description, location: $location, file: $file, fileUrl: $fileUrl) }`,
        variables: {
          title: title,
          description: description,
          location: location,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/house_building.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });
});
