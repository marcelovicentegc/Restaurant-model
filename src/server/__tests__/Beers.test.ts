import { fetch } from "apollo-server-env";
import * as faker from "faker";
import * as FormData from "form-data";
import { createReadStream } from "fs";
import { Response } from "supertest";
const url = `http://127.0.0.1:8080/api/playground`;
const request = require("supertest")(url);

describe(" BEERS TEST ------------------------------------------", () => {
  test("Creates a beer", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const price = parseFloat(faker.commerce.price());

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
          $fileUrl: String
          $title: String!
          $description: String!
          $price: Float!) { createBeer(title: $title, description: $description, price: $price, file: $file, fileUrl: $fileUrl) { title description price fileUrl } }`,
        variables: {
          title: title,
          description: description,
          price: price,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/beer_mug.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });

  test("Returns all beers", async () => {
    try {
      await request
        .post("/")
        .send({ query: "{ beers { id title description price fileUrl } }" })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const beers = res.body.data;
          console.log("Got: " + JSON.stringify(beers));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Returns a single beer", async () => {
    try {
      await request
        .post("/")
        .send({ query: "{ beer(id:1) { title description price fileUrl } }" })
        .set("Accept", "application/json")
        .expect(200)
        .then((res: Response) => {
          const beer = res.body.data;
          console.log("Got: " + JSON.stringify(beer));
        });
    } catch (err) {
      console.log(err);
    }
  });

  test("Updates every property of a beer", async () => {
    const body = new FormData();
    const title = faker.commerce.product();
    const description = faker.lorem.sentence();
    const price = parseFloat(faker.commerce.price());

    body.append(
      "operations",
      JSON.stringify({
        query: `mutation($file: Upload
            $fileUrl: String
            $title: String!
            $description: String!
            $price: Float!) { updateBeer(id:1 title: $title, description: $description, price: $price, file: $file, fileUrl: $fileUrl) }`,
        variables: {
          title: title,
          description: description,
          price: price,
          file: null
        }
      })
    );
    body.append("map", JSON.stringify({ 1: ["variables.file"] }));
    body.append(
      "1",
      createReadStream("src/server/__tests__/assets/beer_mug.png")
    );

    try {
      const resolved = await fetch(url, {
        method: "POST",
        body: body as any
      });
      const text = await resolved.text();
      const response = JSON.parse(text);
      console.log(response);
      expect(200);
    } catch (error) {
      if (error.code !== "EPIPE") throw error;
    }
  });
});
