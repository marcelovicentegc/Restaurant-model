import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("beer")
class Beer extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  title: string;

  @Column()
  description: string;

  @Column({
    type: "float"
  })
  price: number;

  @Column({ nullable: true })
  fileUrl: string;
}

export default Beer;
