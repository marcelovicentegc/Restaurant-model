import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("burger")
class Burger extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  title: string;

  @Column()
  description: string;

  @Column({
    type: "float"
  })
  price: number;

  @Column({ nullable: true })
  fileUrl: string;
}

export default Burger;
