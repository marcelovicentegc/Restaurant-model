import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("user")
class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "varchar", length: "100" })
  fullName: string;

  @Column({ type: "varchar", length: "50", unique: true })
  email: string;

  @Column({ type: "varchar", length: "60" })
  password: string;
}

export default User;
