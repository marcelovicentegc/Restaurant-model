import About from "./About.model";
import Beer from "./Beer.model";
import Burger from "./Burger.model";
import Product from "./Product.model";
import Snack from "./Snack.model";
import User from "./User.model";

export { User, Beer, Burger, Snack, About, Product };
