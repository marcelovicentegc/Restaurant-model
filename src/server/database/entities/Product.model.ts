import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("product")
class Product extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;

  @Column({ unique: true })
  title: string;

  @Column()
  description: string;

  @Column({
    type: "float"
  })
  price: number;

  @Column()
  location: string;

  @Column({ nullable: true })
  fileUrl: string;
}

export default Product;
