import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("about")
class About extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  title: string;

  @Column()
  description: string;

  @Column()
  location: string;

  @Column({ nullable: true })
  fileUrl: string;
}

export default About;
