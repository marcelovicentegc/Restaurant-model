import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AdminConnector from "../modules/admin/AdminConnector";
import HomeConnector from "../modules/home/HomeConnector";
import LoginConnector from "../modules/login/LoginConnector";

export const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path="/" component={HomeConnector} />
        <Route exact={true} path="/admin" component={AdminConnector} />
        <Route exact={true} path="/login" component={LoginConnector} />
      </Switch>
    </BrowserRouter>
  );
};
