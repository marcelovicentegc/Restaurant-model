import { shallow } from "enzyme";
import * as React from "react";
import { Query } from "react-apollo";
import { Redirect } from "react-router";
import {
  getAbouts,
  getBeers,
  getBurgers,
  getSnacks,
  getUser
} from "../../server/schema/graphql/Queries.graphql";
import AdminView from "../modules/admin/ui/AdminView";
import NavBar from "../modules/admin/ui/home/NavBar";
import Products from "../modules/admin/ui/home/Products";
import Loading from "../modules/misc/AdminLoading";
import { GetUserQuery } from "../__types__/apollo";
import "./main.scss";

describe("Admin suite", () => {
  it("Should render without throwing an error", () => {
    expect(
      shallow(<AdminView />).contains(
        <Query<GetUserQuery> fetchPolicy="network-only" query={getUser}>
          {({ data, loading }) => {
            if (loading) {
              <Loading />;
            }

            if (!data) {
              return <Redirect to="/login" />;
            }

            return (
              <>
                <NavBar />
                <Products
                  abouts={getAbouts}
                  burgers={getBurgers}
                  snacks={getSnacks}
                  beers={getBeers}
                />
              </>
            );
          }}
        </Query>
      )
    ).toBe(true);
  });
});
