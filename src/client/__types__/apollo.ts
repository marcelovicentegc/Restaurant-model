export type Maybe<T> = T | null;

export type Upload = any;

// ====================================================
// Documents
// ====================================================

export type CreateUserVariables = {
  fullName: string;
  email: string;
  password: string;
};

export type CreateUserMutation = {
  __typename?: "Mutation";

  createUser: CreateUserCreateUser;
};

export type CreateUserCreateUser = {
  __typename?: "User";

  fullName: string;

  email: string;

  password: string;
};

export type LoginUserVariables = {
  email: string;
  password: string;
};

export type LoginUserMutation = {
  __typename?: "Mutation";

  loginUser: LoginUserLoginUser;
};

export type LoginUserLoginUser = {
  __typename?: "User";

  id: string;

  email: string;
};

export type LogoutUserVariables = {};

export type LogoutUserMutation = {
  __typename?: "Mutation";

  logoutUser: boolean;
};

export type CreateAboutVariables = {
  title: string;
  file?: Maybe<Upload>;
  fileUrl?: Maybe<string>;
  description: string;
  location: string;
};

export type CreateAboutMutation = {
  __typename?: "Mutation";

  createAbout: CreateAboutCreateAbout;
};

export type CreateAboutCreateAbout = {
  __typename?: "About";

  title: string;

  fileUrl: Maybe<string>;

  description: string;

  location: string;
};

export type UpdateAboutVariables = {
  id: string;
  file?: Maybe<Upload>;
  title: string;
  description: string;
  location: string;
};

export type UpdateAboutMutation = {
  __typename?: "Mutation";

  updateAbout: boolean;
};

export type DeleteAboutVariables = {
  id: string;
};

export type DeleteAboutMutation = {
  __typename?: "Mutation";

  deleteAbout: boolean;
};

export type CreateBurgerVariables = {
  file?: Maybe<Upload>;
  fileUrl?: Maybe<string>;
  title: string;
  description: string;
  price: number;
};

export type CreateBurgerMutation = {
  __typename?: "Mutation";

  createBurger: CreateBurgerCreateBurger;
};

export type CreateBurgerCreateBurger = {
  __typename?: "Burger";

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type UpdateBurgerVariables = {
  id: string;
  file?: Maybe<Upload>;
  title: string;
  description: string;
  price: number;
};

export type UpdateBurgerMutation = {
  __typename?: "Mutation";

  updateBurger: boolean;
};

export type DeleteBurgerVariables = {
  id: string;
};

export type DeleteBurgerMutation = {
  __typename?: "Mutation";

  deleteBurger: boolean;
};

export type CreateSnackVariables = {
  title: string;
  file?: Maybe<Upload>;
  fileUrl?: Maybe<string>;
  description: string;
  price: number;
};

export type CreateSnackMutation = {
  __typename?: "Mutation";

  createSnack: CreateSnackCreateSnack;
};

export type CreateSnackCreateSnack = {
  __typename?: "Snack";

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type UpdateSnackVariables = {
  id: string;
  file?: Maybe<Upload>;
  title: string;
  description: string;
  price: number;
};

export type UpdateSnackMutation = {
  __typename?: "Mutation";

  updateSnack: boolean;
};

export type DeleteSnackVariables = {
  id: string;
};

export type DeleteSnackMutation = {
  __typename?: "Mutation";

  deleteSnack: boolean;
};

export type CreateBeerVariables = {
  title: string;
  file?: Maybe<Upload>;
  fileUrl?: Maybe<string>;
  description: string;
  price: number;
};

export type CreateBeerMutation = {
  __typename?: "Mutation";

  createBeer: CreateBeerCreateBeer;
};

export type CreateBeerCreateBeer = {
  __typename?: "Beer";

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type UpdateBeerVariables = {
  id: string;
  file?: Maybe<Upload>;
  title: string;
  description: string;
  price: number;
};

export type UpdateBeerMutation = {
  __typename?: "Mutation";

  updateBeer: boolean;
};

export type DeleteBeerVariables = {
  id: string;
};

export type DeleteBeerMutation = {
  __typename?: "Mutation";

  deleteBeer: boolean;
};

export type GetUserVariables = {};

export type GetUserQuery = {
  __typename?: "Query";

  user: GetUserUser;
};

export type GetUserUser = {
  __typename?: "User";

  id: string;

  email: string;

  fullName: string;
};

export type GetAboutsVariables = {};

export type GetAboutsQuery = {
  __typename?: "Query";

  abouts: GetAboutsAbouts[];
};

export type GetAboutsAbouts = {
  __typename?: "About";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  location: string;
};

export type GetAboutVariables = {
  id: string;
};

export type GetAboutQuery = {
  __typename?: "Query";

  about: GetAboutAbout;
};

export type GetAboutAbout = {
  __typename?: "About";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  location: string;
};

export type GetBurgersVariables = {};

export type GetBurgersQuery = {
  __typename?: "Query";

  burgers: GetBurgersBurgers[];
};

export type GetBurgersBurgers = {
  __typename?: "Burger";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type GetBurgerVariables = {
  id: string;
};

export type GetBurgerQuery = {
  __typename?: "Query";

  burger: GetBurgerBurger;
};

export type GetBurgerBurger = {
  __typename?: "Burger";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type GetSnacksVariables = {};

export type GetSnacksQuery = {
  __typename?: "Query";

  snacks: GetSnacksSnacks[];
};

export type GetSnacksSnacks = {
  __typename?: "Snack";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type GetSnackVariables = {
  id: string;
};

export type GetSnackQuery = {
  __typename?: "Query";

  snack: GetSnackSnack;
};

export type GetSnackSnack = {
  __typename?: "Snack";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type GetBeersVariables = {};

export type GetBeersQuery = {
  __typename?: "Query";

  beers: GetBeersBeers[];
};

export type GetBeersBeers = {
  __typename?: "Beer";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};

export type GetBeerVariables = {
  id: string;
};

export type GetBeerQuery = {
  __typename?: "Query";

  beer: GetBeerBeer;
};

export type GetBeerBeer = {
  __typename?: "Beer";

  id: string;

  fileUrl: Maybe<string>;

  title: string;

  description: string;

  price: number;
};
