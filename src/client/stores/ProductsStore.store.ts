import { action, observable } from "mobx";
import { RootStore } from "./RootStore.store";

export class ProductsStore {
  protected rootStore: RootStore;

  public constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @observable public selectedAbout: string = null;
  @observable public selectedBurger: string = null;
  @observable public selectedSnack: string = null;
  @observable public selectedBeer: string = null;

  @observable public updateAbout: boolean = false;
  @observable public updateBurger: boolean = false;
  @observable public updateSnack: boolean = false;
  @observable public updateBeer: boolean = false;

  @observable public deleteAbout: boolean = false;
  @observable public deleteBurger: boolean = false;
  @observable public deleteSnack: boolean = false;
  @observable public deleteBeer: boolean = false;

  @observable public aboutList: boolean = true;
  @observable public burgerList: boolean = true;
  @observable public snackList: boolean = true;
  @observable public beerList: boolean = true;

  @action public updateAboutController = () => {
    this.updateAbout ? (this.updateAbout = false) : (this.updateAbout = true);
  };
  @action public deleteAboutController = () => {
    this.deleteAbout ? (this.deleteAbout = false) : (this.deleteAbout = true);
  };

  @action public updateBurgerController = () => {
    this.updateBurger
      ? (this.updateBurger = false)
      : (this.updateBurger = true);
  };
  @action public deleteBurgerController = () => {
    this.deleteBurger
      ? (this.deleteBurger = false)
      : (this.deleteBurger = true);
  };

  @action public updateSnackController = () => {
    this.updateSnack ? (this.updateSnack = false) : (this.updateSnack = true);
  };
  @action public deleteSnackController = () => {
    this.deleteSnack ? (this.deleteSnack = false) : (this.deleteSnack = true);
  };

  @action public updateBeerController = () => {
    this.updateBeer ? (this.updateBeer = false) : (this.updateBeer = true);
  };
  @action public deleteBeerController = () => {
    this.deleteBeer ? (this.deleteBeer = false) : (this.deleteBeer = true);
  };

  @action public handleAbout = () => {
    this.aboutList ? (this.aboutList = false) : (this.aboutList = true);
  };

  @action public handleBurger = () => {
    this.burgerList ? (this.burgerList = false) : (this.burgerList = true);
  };

  @action public handleSnack = () => {
    this.snackList ? (this.snackList = false) : (this.snackList = true);
  };

  @action public handleBeer = () => {
    this.beerList ? (this.beerList = false) : (this.beerList = true);
  };
}
