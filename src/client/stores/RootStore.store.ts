import { ProductsStore } from "./ProductsStore.store";

export class RootStore {
  public productsStore: ProductsStore;

  public constructor() {
    this.productsStore = new ProductsStore(this);

    return {
      productsStore: this.productsStore
    };
  }
}
