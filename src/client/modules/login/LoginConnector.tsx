import * as React from "react";
import LoginView from "./ui/LoginView";

const LoginConnector = () => <LoginView />;
export default LoginConnector;
