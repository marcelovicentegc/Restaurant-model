import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { loginUser } from "../../../../server/schema/graphql/Mutations.graphql";
import {
  LoginUserMutation,
  LoginUserVariables
} from "../../../__types__/apollo";
import "./main.scss";

class LoginView extends React.Component<RouteComponentProps<{}>> {
  state = {
    email: "",
    password: ""
  };

  submitForm(e: Event) {
    e.preventDefault();
  }

  render() {
    return (
      <Mutation<LoginUserMutation, LoginUserVariables> mutation={loginUser}>
        {mutate => (
          <>
            <div className="login-form-wrapper">
              <div className="form-header">
                <p>The Restaurant admin</p>
              </div>
              <div className="login-form">
                <form className="form" onSubmit={this.submitForm.bind(this)}>
                  <div className="form-field">
                    <label>Email:</label>
                    <input
                      type="text"
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                  </div>

                  <div className="form-field">
                    <label>Password:</label>
                    <input
                      type="password"
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                  </div>

                  <div className="button-wrapper">
                    <button
                      onClick={async () => {
                        const response = await mutate({
                          variables: {
                            email: this.state.email,
                            password: this.state.password
                          }
                        });
                        console.log(response);
                        this.props.history.push("/admin");
                      }}
                    >
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </>
        )}
      </Mutation>
    );
  }
}

export default withRouter(LoginView);
