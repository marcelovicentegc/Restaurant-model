import * as React from "react";
import AdminView from "./ui/AdminView";

const AdminConnector = () => <AdminView />;
export default AdminConnector;
