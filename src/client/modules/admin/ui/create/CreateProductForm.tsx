import * as React from "react";
import CreateAbout from "./CreateAbout";
import CreateBeer from "./CreateBeer";
import CreateBurger from "./CreateBurger";
import CreateSnack from "./CreateSnack";

interface Props {
  currentProduct: string;
  handleForm(): void;
}

export default class CreateProductForm extends React.Component<Props> {
  displayForm() {
    if (this.props.currentProduct === "about") {
      return <CreateAbout />;
    }
    if (this.props.currentProduct === "burger") {
      return <CreateBurger />;
    }
    if (this.props.currentProduct === "snack") {
      return <CreateSnack />;
    }
    if (this.props.currentProduct === "beer") {
      return <CreateBeer />;
    }
  }

  render() {
    return (
      <>
        {this.displayForm()}
        <div className="back-button-wrapper">
          <button onClick={this.props.handleForm} />
        </div>
      </>
    );
  }
}
