import * as React from "react";
import CreateProductWrapper from "./CreateProductWrapper";

interface Props {}

interface State {
  popFormUp: boolean;
}

export default class CreateButton extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      popFormUp: false
    };
  }

  toggleForm() {
    this.setState({
      popFormUp: !this.state.popFormUp
    });
  }

  render() {
    return (
      <>
        <div className="create-product-button-wrapper">
          <button onClick={this.toggleForm.bind(this)}>
            <span>Create</span>
          </button>
          {this.state.popFormUp ? (
            <CreateProductWrapper handleForm={this.toggleForm.bind(this)} />
          ) : null}
        </div>
      </>
    );
  }
}
