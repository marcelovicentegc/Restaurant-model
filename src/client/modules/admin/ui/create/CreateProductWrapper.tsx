import * as React from "react";
import CreateProductForm from "./CreateProductForm";

interface Props {
  handleForm: any;
}

interface State {
  showAboutForm: boolean;
  showBurgerForm: boolean;
  showSnackForm: boolean;
  showBeerForm: boolean;
}

export default class CreateProductWrapper extends React.PureComponent<
  Props,
  State
> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showAboutForm: false,
      showBurgerForm: false,
      showSnackForm: false,
      showBeerForm: false
    };

    this.handleAboutForm = this.handleAboutForm.bind(this);
    this.handleBurgerForm = this.handleBurgerForm.bind(this);
    this.handleSnackForm = this.handleSnackForm.bind(this);
    this.handleBeerForm = this.handleBeerForm.bind(this);
  }

  handleAboutForm() {
    if (this.state.showAboutForm) {
      this.setState({
        showAboutForm: false
      });
    } else {
      this.setState({
        showAboutForm: true
      });
    }
  }

  handleBurgerForm() {
    if (this.state.showBurgerForm) {
      this.setState({
        showBurgerForm: false
      });
    } else {
      this.setState({
        showBurgerForm: true
      });
    }
  }

  handleSnackForm() {
    if (this.state.showSnackForm) {
      this.setState({
        showSnackForm: false
      });
    } else {
      this.setState({
        showSnackForm: true
      });
    }
  }

  handleBeerForm() {
    if (this.state.showBeerForm) {
      this.setState({
        showBeerForm: false
      });
    } else {
      this.setState({
        showBeerForm: true
      });
    }
  }

  render() {
    return (
      <>
        <div className="form-wrapper">
          <div className="form">
            <div className="create-new-product">
              <div id="about" className="product">
                <p onClick={this.handleAboutForm}>About</p>
                {this.state.showAboutForm ? (
                  <CreateProductForm
                    currentProduct={"about"}
                    handleForm={this.handleAboutForm}
                  />
                ) : null}
              </div>
              <div id="burger" className="product">
                <p onClick={this.handleBurgerForm}>Burger</p>
                {this.state.showBurgerForm ? (
                  <CreateProductForm
                    currentProduct={"burger"}
                    handleForm={this.handleBurgerForm}
                  />
                ) : null}
              </div>
              <div id="snack" className="product">
                <p onClick={this.handleSnackForm}>Snack</p>
                {this.state.showSnackForm ? (
                  <CreateProductForm
                    currentProduct={"snack"}
                    handleForm={this.handleSnackForm}
                  />
                ) : null}
              </div>
              <div id="beer" className="product">
                <p onClick={this.handleBeerForm}>Beer</p>
                {this.state.showBeerForm ? (
                  <CreateProductForm
                    currentProduct={"beer"}
                    handleForm={this.handleBeerForm}
                  />
                ) : null}
              </div>
            </div>
            <div className="back-wrapper-button-wrapper">
              <button onClick={this.props.handleForm} />
            </div>
          </div>
        </div>
      </>
    );
  }
}
