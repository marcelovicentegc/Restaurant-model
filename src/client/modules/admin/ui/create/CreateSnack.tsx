import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { createSnack } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getSnacks } from "../../../../../server/schema/graphql/Queries.graphql";
import {
  CreateSnackMutation,
  CreateSnackVariables
} from "../../../../__types__/apollo";

interface Props {}

interface State {
  file: File;
  fileUrl: string | null;
  title: string;
  description: string;
  price: number;
}

export default class CreateSnack extends React.PureComponent<Props, State> {
  private input: React.RefObject<HTMLInputElement>;
  constructor(props: Props) {
    super(props);

    this.state = {
      file: undefined,
      fileUrl: null,
      title: undefined,
      description: undefined,
      price: undefined
    };

    this.input = React.createRef<HTMLInputElement>();
    this.loadPic = this.loadPic.bind(this);
  }

  picInput() {
    this.input.current.click();
  }

  loadPic() {
    return this.state.fileUrl != null
      ? this.state.fileUrl
      : "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/microsoft/153/hot-pepper_1f336.png";
  }

  render() {
    return (
      <Mutation<CreateSnackMutation, CreateSnackVariables>
        mutation={createSnack}
        refetchQueries={[{ query: getSnacks }]}
        awaitRefetchQueries={true}
      >
        {mutate => (
          <>
            <form id="create-snack" encType="multipart/form-data">
              <div className="field img-field">
                <img src={this.loadPic()} onClick={this.picInput.bind(this)} />
                <input
                  ref={this.input}
                  type="file"
                  onChange={e =>
                    this.setState({
                      fileUrl: URL.createObjectURL(e.target.files[0]),
                      file: e.target.files[0]
                    }) + this.loadPic()
                  }
                />
              </div>
              <div className="field">
                <label>Title:</label>
                <input
                  type="text"
                  placeholder="Name"
                  onChange={e => this.setState({ title: e.target.value })}
                />
              </div>

              <div className="field description">
                <label>Description:</label>
                <textarea
                  placeholder="Description"
                  onChange={e => this.setState({ description: e.target.value })}
                />
              </div>

              <div className="field">
                <label>Price:</label>
                <input
                  type="number"
                  placeholder="$"
                  onChange={e =>
                    this.setState({ price: parseFloat(e.target.value) })
                  }
                />
              </div>

              <button
                className="submit-button"
                onClick={async () => {
                  await mutate({
                    variables: {
                      file: this.state.file,
                      title: this.state.title,
                      description: this.state.description,
                      price: this.state.price
                    }
                  });
                }}
              />
            </form>
          </>
        )}
      </Mutation>
    );
  }
}
