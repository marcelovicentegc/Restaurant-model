import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { createAbout } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getAbouts } from "../../../../../server/schema/graphql/Queries.graphql";
import {
  CreateAboutMutation,
  CreateAboutVariables
} from "../../../../__types__/apollo";

interface Props {}

interface State {
  file: File;
  fileUrl: string | null;
  title: string;
  description: string;
  location: string;
}

export default class CreateAbout extends React.PureComponent<Props, State> {
  private input: React.RefObject<HTMLInputElement>;
  constructor(props: Props) {
    super(props);

    this.state = {
      file: undefined,
      fileUrl: null,
      title: undefined,
      description: undefined,
      location: undefined
    };

    this.input = React.createRef<HTMLInputElement>();
    this.loadPic = this.loadPic.bind(this);
  }

  picInput() {
    this.input.current.click();
  }

  loadPic() {
    return this.state.fileUrl != null
      ? this.state.fileUrl
      : "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/microsoft/153/house-building_1f3e0.png";
  }

  render() {
    return (
      <Mutation<CreateAboutMutation, CreateAboutVariables>
        mutation={createAbout}
        refetchQueries={[{ query: getAbouts }]}
        awaitRefetchQueries={true}
      >
        {mutate => (
          <>
            <form id="create-about" encType="multipart/form-data">
              <div className="field img-field">
                <img src={this.loadPic()} onClick={this.picInput.bind(this)} />
                <input
                  ref={this.input}
                  type="file"
                  onChange={e =>
                    this.setState({
                      fileUrl: URL.createObjectURL(e.target.files[0]),
                      file: e.target.files[0]
                    }) + this.loadPic()
                  }
                />
              </div>

              <div className="field">
                <label>Title:</label>
                <input
                  type="text"
                  placeholder="Name"
                  onChange={e => this.setState({ title: e.target.value })}
                />
              </div>

              <div className="field description">
                <label>Description:</label>
                <textarea
                  placeholder="Description"
                  onChange={e => this.setState({ description: e.target.value })}
                />
              </div>

              <div className="field">
                <label>Location:</label>
                <input
                  type="text"
                  placeholder="Location"
                  onChange={e => this.setState({ location: e.target.value })}
                />
              </div>

              <button
                className="submit-button"
                onClick={async () => {
                  await mutate({
                    variables: {
                      file: this.state.file,
                      title: this.state.title,
                      description: this.state.description,
                      location: this.state.location
                    }
                  });
                }}
              />
            </form>
          </>
        )}
      </Mutation>
    );
  }
}
