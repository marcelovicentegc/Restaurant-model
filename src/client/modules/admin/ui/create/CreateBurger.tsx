import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { createBurger } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getBurgers } from "../../../../../server/schema/graphql/Queries.graphql";
import {
  CreateBurgerMutation,
  CreateBurgerVariables
} from "../../../../__types__/apollo";

interface Props {}

interface State {
  file: File;
  fileUrl: string | null;
  title: string;
  description: string;
  price: number;
}

export default class CreateBurger extends React.Component<Props, State> {
  private input: React.RefObject<HTMLInputElement>;
  constructor(props: Props) {
    super(props);

    this.state = {
      file: undefined,
      fileUrl: null,
      title: undefined,
      description: undefined,
      price: undefined
    };

    this.input = React.createRef<HTMLInputElement>();
    this.loadPic = this.loadPic.bind(this);
  }

  picInput() {
    this.input.current.click();
  }

  loadPic() {
    return this.state.fileUrl != null
      ? this.state.fileUrl
      : "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/microsoft/153/hamburger_1f354.png";
  }

  render() {
    return (
      <Mutation<CreateBurgerMutation, CreateBurgerVariables>
        mutation={createBurger}
        refetchQueries={[{ query: getBurgers }]}
        awaitRefetchQueries={true}
      >
        {mutate => (
          <>
            <form id="create-burger" encType="multipart/form-data">
              <div className="field img-field">
                <img src={this.loadPic()} onClick={this.picInput.bind(this)} />
                <input
                  ref={this.input}
                  type="file"
                  onChange={e =>
                    this.setState({
                      fileUrl: URL.createObjectURL(e.target.files[0]),
                      file: e.target.files[0]
                    }) + this.loadPic()
                  }
                />
              </div>

              <div className="field">
                <input
                  type="text"
                  placeholder="Name"
                  onChange={e => this.setState({ title: e.target.value })}
                />
              </div>

              <div className="field description">
                <textarea
                  placeholder="Description"
                  onChange={e => this.setState({ description: e.target.value })}
                />
              </div>

              <div className="field">
                <input
                  type="number"
                  placeholder="$"
                  onChange={e =>
                    this.setState({ price: parseFloat(e.target.value) })
                  }
                />
              </div>

              <button
                className="submit-button"
                onClick={async () => {
                  await mutate({
                    variables: {
                      file: this.state.file,
                      title: this.state.title,
                      description: this.state.description,
                      price: this.state.price
                    }
                  });
                }}
              />
            </form>
          </>
        )}
      </Mutation>
    );
  }
}
