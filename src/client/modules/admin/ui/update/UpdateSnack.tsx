import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { updateSnack } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getSnacks } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  GetSnackSnack,
  UpdateSnackMutation,
  UpdateSnackVariables
} from "../../../../__types__/apollo";
import BackButton from "../home/BackButton";
import ProductDetails from "../home/ProductDetails";

interface Props {
  product: GetSnackSnack;
  productsStore?: ProductsStore;
}

interface State {
  file: File;
  fileUrl: string | null;
  id: string;
  title: string;
  description: string;
  price: number;
}

@inject("productsStore")
@observer
export default class UpdateSnack extends React.Component<Props, State> {
  private updateSnackController = () => {
    this.props.productsStore.updateSnackController();
  };

  private input: React.RefObject<HTMLInputElement>;
  constructor(props: Props) {
    super(props);

    this.state = {
      fileUrl: null,
      file: undefined,
      id: undefined,
      title: undefined,
      description: undefined,
      price: undefined
    };

    this.input = React.createRef<HTMLInputElement>();
    this.loadPic = this.loadPic.bind(this);
  }

  picInput() {
    this.input.current.click();
  }

  loadPic() {
    return this.state.fileUrl != null
      ? this.state.fileUrl
      : this.props.product.fileUrl;
  }

  render() {
    return (
      <>
        {this.props.productsStore.updateSnack ? (
          <Mutation<UpdateSnackMutation, UpdateSnackVariables>
            mutation={updateSnack}
            refetchQueries={[{ query: getSnacks }]}
            awaitRefetchQueries={true}
          >
            {mutate => (
              <form className="product-detail">
                <div className="product-title">
                  <textarea
                    rows={1}
                    placeholder={this.props.product.title}
                    onChange={e => this.setState({ title: e.target.value })}
                  />
                </div>
                <div className="product-description">
                  <textarea
                    placeholder={this.props.product.description}
                    onChange={e =>
                      this.setState({ description: e.target.value })
                    }
                  />
                </div>
                <div className="product-comp">
                  <textarea
                    rows={1}
                    placeholder={"$" + this.props.product.price.toString()}
                    onChange={e =>
                      this.setState({ price: parseFloat(e.target.value) })
                    }
                  />
                </div>
                <div className="product-pic">
                  <input
                    type="file"
                    ref={this.input}
                    onChange={e =>
                      this.setState({
                        fileUrl: URL.createObjectURL(e.target.files[0]),
                        file: e.target.files[0]
                      }) + this.loadPic()
                    }
                  />
                  <div className="on-hover" onClick={this.picInput.bind(this)}>
                    <p>Chose file</p>
                  </div>
                  <img src={this.loadPic()} />
                </div>
                <button
                  className="update-button"
                  onClick={async () => {
                    await mutate({
                      variables: {
                        id: this.props.product.id,
                        file: this.state.file,
                        title: this.state.title || this.props.product.title,
                        description:
                          this.state.description ||
                          this.props.product.description,
                        price: this.state.price || this.props.product.price
                      }
                    });
                  }}
                >
                  <span>Submit!</span>
                </button>
                <BackButton
                  goBack={this.updateSnackController}
                  atUpdate="back-button-at-update"
                />
              </form>
            )}
          </Mutation>
        ) : (
          <ProductDetails snack={this.props.product} />
        )}
      </>
    );
  }
}
