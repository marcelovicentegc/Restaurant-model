import * as React from "react";

interface Props {
  updateController(): void;
}

const UpdateButton = (props: Props) => {
  return (
    <button className="update-button" onClick={props.updateController}>
      <span>Edit</span>
    </button>
  );
};

export default UpdateButton;
