import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { updateAbout } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getAbouts } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  GetAboutAbout,
  UpdateAboutMutation,
  UpdateAboutVariables
} from "../../../../__types__/apollo";
import BackButton from "../home/BackButton";
import ProductDetails from "../home/ProductDetails";

interface Props {
  product: GetAboutAbout;
  productsStore?: ProductsStore;
}

interface State {
  file: File;
  fileUrl: string | null;
  id: string;
  title: string;
  description: string;
  location: string;
}

@inject("productsStore")
@observer
export default class UpdateAbout extends React.Component<Props, State> {
  private updateAboutController = () => {
    this.props.productsStore.updateAboutController();
  };

  private input: React.RefObject<HTMLInputElement>;
  constructor(props: Props) {
    super(props);

    this.state = {
      fileUrl: null,
      file: undefined,
      id: undefined,
      title: undefined,
      description: undefined,
      location: undefined
    };

    this.input = React.createRef<HTMLInputElement>();
    this.loadPic = this.loadPic.bind(this);
  }

  picInput() {
    this.input.current.click();
  }

  loadPic() {
    return this.state.fileUrl != null
      ? this.state.fileUrl
      : this.props.product.fileUrl;
  }

  render() {
    console.log(this.input);
    return (
      <>
        {this.props.productsStore.updateAbout ? (
          <>
            <Mutation<UpdateAboutMutation, UpdateAboutVariables>
              mutation={updateAbout}
              refetchQueries={[{ query: getAbouts }]}
              awaitRefetchQueries={true}
            >
              {mutate => (
                <form className="product-detail">
                  <div className="product-title">
                    <textarea
                      rows={1}
                      placeholder={this.props.product.title}
                      onChange={e => this.setState({ title: e.target.value })}
                    />
                  </div>
                  <div className="product-description">
                    <textarea
                      placeholder={this.props.product.description}
                      onChange={e =>
                        this.setState({ description: e.target.value })
                      }
                    />
                  </div>
                  <div className="product-comp">
                    <textarea
                      rows={1}
                      placeholder={"@" + this.props.product.location}
                      onChange={e =>
                        this.setState({ location: e.target.value })
                      }
                    />
                  </div>
                  <div className="product-pic">
                    <input
                      type="file"
                      ref={this.input}
                      onChange={e =>
                        this.setState({
                          fileUrl: URL.createObjectURL(e.target.files[0]),
                          file: e.target.files[0]
                        }) + this.loadPic()
                      }
                    />
                    <div
                      className="on-hover"
                      onClick={this.picInput.bind(this)}
                    >
                      <p>Chose file</p>
                    </div>
                    <img src={this.loadPic()} />
                  </div>
                  <button
                    className="update-button"
                    onClick={async () => {
                      await mutate({
                        variables: {
                          id: this.props.product.id,
                          file: this.state.file,
                          title: this.state.title || this.props.product.title,
                          description:
                            this.state.description ||
                            this.props.product.description,
                          location:
                            this.state.location || this.props.product.location
                        }
                      });
                    }}
                  >
                    <span>Submit!</span>
                  </button>
                  <BackButton
                    goBack={this.updateAboutController}
                    atUpdate="back-button-at-update"
                  />
                </form>
              )}
            </Mutation>
          </>
        ) : (
          <ProductDetails about={this.props.product} />
        )}
      </>
    );
  }
}
