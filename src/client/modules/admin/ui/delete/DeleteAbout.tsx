import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { deleteAbout } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getAbouts } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  DeleteAboutMutation,
  DeleteAboutVariables,
  GetAboutAbout
} from "../../../../__types__/apollo";

interface Props {
  product: GetAboutAbout;
  productsStore?: ProductsStore;
}

@inject("productsStore")
@observer
export default class DeleteAbout extends React.Component<Props> {
  private deleteAboutController = () => {
    this.props.productsStore.deleteAboutController();
  };

  render() {
    return (
      <div className="form-wrapper">
        <div className="form">
          <div className="delete-form">
            <h2>
              Are you sure you want to delete this product? This action will be
              irreversible.
            </h2>
            <div className="options">
              <Mutation<DeleteAboutMutation, DeleteAboutVariables>
                mutation={deleteAbout}
                refetchQueries={[{ query: getAbouts }]}
                awaitRefetchQueries={true}
              >
                {mutate => (
                  <h1
                    className="delete"
                    onClick={async () => {
                      await mutate({
                        variables: {
                          id: this.props.product.id
                        }
                      }).then(() => window.location.reload());
                    }}
                  >
                    Yes
                  </h1>
                )}
              </Mutation>
              <h1 className="back" onClick={this.deleteAboutController}>
                No
              </h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
