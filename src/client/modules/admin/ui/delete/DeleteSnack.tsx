import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { deleteSnack } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getSnacks } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  DeleteSnackMutation,
  DeleteSnackVariables,
  GetSnackSnack
} from "../../../../__types__/apollo";

interface Props {
  product: GetSnackSnack;
  productsStore?: ProductsStore;
}

@inject("productsStore")
@observer
export default class DeleteSnack extends React.Component<Props> {
  private deleteSnackController = () => {
    this.props.productsStore.deleteSnackController();
  };

  render() {
    return (
      <div className="form-wrapper">
        <div className="form">
          <div className="delete-form">
            <h2>
              Are you sure you want to delete this product? This action will be
              irreversible.
            </h2>
            <div className="options">
              <Mutation<DeleteSnackMutation, DeleteSnackVariables>
                mutation={deleteSnack}
                refetchQueries={[{ query: getSnacks }]}
                awaitRefetchQueries={true}
              >
                {mutate => (
                  <h1
                    className="delete"
                    onClick={async () => {
                      await mutate({
                        variables: {
                          id: this.props.product.id
                        }
                      }).then(() => window.location.reload());
                    }}
                  >
                    Yes
                  </h1>
                )}
              </Mutation>
              <h1 className="back" onClick={this.deleteSnackController}>
                No
              </h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
