import * as React from "react";

interface Props {
  deleteController(): void;
}

const DeleteButton = (props: Props) => {
  return (
    <button className="delete-button" onClick={props.deleteController}>
      <span>Delete</span>
    </button>
  );
};

export default DeleteButton;
