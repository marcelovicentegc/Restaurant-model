import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { deleteBurger } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getBurgers } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  DeleteBurgerMutation,
  DeleteBurgerVariables,
  GetBurgerBurger
} from "../../../../__types__/apollo";

interface Props {
  product: GetBurgerBurger;
  productsStore?: ProductsStore;
}

@inject("productsStore")
@observer
export default class DeleteBurger extends React.Component<Props> {
  private deleteBurgerController = () => {
    this.props.productsStore.deleteBurgerController();
  };

  render() {
    return (
      <div className="form-wrapper">
        <div className="form">
          <div className="delete-form">
            <h2>
              Are you sure you want to delete this product? This action will be
              irreversible.
            </h2>
            <div className="options">
              <Mutation<DeleteBurgerMutation, DeleteBurgerVariables>
                mutation={deleteBurger}
                refetchQueries={[{ query: getBurgers }]}
                awaitRefetchQueries={true}
              >
                {mutate => (
                  <h1
                    className="delete"
                    onClick={async () => {
                      await mutate({
                        variables: {
                          id: this.props.product.id
                        }
                      }).then(() => window.location.reload());
                    }}
                  >
                    Yes
                  </h1>
                )}
              </Mutation>
              <h1 className="back" onClick={this.deleteBurgerController}>
                No
              </h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
