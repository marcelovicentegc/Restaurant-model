import { inject, observer } from "mobx-react";
import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { deleteBeer } from "../../../../../server/schema/graphql/Mutations.graphql";
import { getBeers } from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  DeleteBeerMutation,
  DeleteBeerVariables,
  GetBeerBeer
} from "../../../../__types__/apollo";

interface Props {
  product: GetBeerBeer;
  productsStore?: ProductsStore;
}

@inject("productsStore")
@observer
export default class DeleteBeer extends React.Component<Props> {
  private deleteBeerController = () => {
    this.props.productsStore.deleteBeerController();
  };

  render() {
    return (
      <div className="form-wrapper">
        <div className="form">
          <div className="delete-form">
            <h2>
              {" "}
              Are you sure you want to delete this product? This action will be
              irreversible.
            </h2>
            <div className="options">
              <Mutation<DeleteBeerMutation, DeleteBeerVariables>
                mutation={deleteBeer}
                refetchQueries={[{ query: getBeers }]}
                awaitRefetchQueries={true}
              >
                {mutate => (
                  <h1
                    className="delete"
                    onClick={async () => {
                      await mutate({
                        variables: {
                          id: this.props.product.id
                        }
                      }).then(() => window.location.reload());
                    }}
                  >
                    Yes
                  </h1>
                )}
              </Mutation>
              <h1 className="back" onClick={this.deleteBeerController}>
                No
              </h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
