import * as React from "react";
import Query from "react-apollo/Query";
import { Redirect } from "react-router-dom";
import {
  getAbouts,
  getBeers,
  getBurgers,
  getSnacks,
  getUser
} from "../../../../server/schema/graphql/Queries.graphql";
import { GetUserQuery } from "../../../__types__/apollo";
import Loading from "../../misc/AdminLoading";
import NavBar from "./home/NavBar";
import Products from "./home/Products";
import "./main.scss";

const AdminView = () => {
  return (
    <Query<GetUserQuery> fetchPolicy="network-only" query={getUser}>
      {({ data, loading }) => {
        if (loading) {
          <Loading />;
        }

        if (!data) {
          return <Redirect to="/login" />;
        }

        return (
          <>
            <NavBar />
            <Products
              abouts={getAbouts}
              burgers={getBurgers}
              snacks={getSnacks}
              beers={getBeers}
            />
          </>
        );
      }}
    </Query>
  );
};

export default AdminView;
