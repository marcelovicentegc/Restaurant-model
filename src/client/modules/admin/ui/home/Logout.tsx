import * as React from "react";
import Mutation from "react-apollo/Mutation";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { logoutUser } from "../../../../../server/schema/graphql/Mutations.graphql";
import { LogoutUserMutation } from "../../../../__types__/apollo";

class Logout extends React.Component<RouteComponentProps<{}>> {
  render() {
    return (
      <>
        <Mutation<LogoutUserMutation> mutation={logoutUser}>
          {(mutate, { client }) => (
            <p
              className="nav-item"
              onClick={async () => {
                const response = await mutate();
                await client.resetStore();
                console.log(response);
                this.props.history.push("/");
              }}
            >
              Log out
            </p>
          )}
        </Mutation>
      </>
    );
  }
}

export default withRouter(Logout);
