import { inject, observer } from "mobx-react";
import * as React from "react";
import Query from "react-apollo/Query";
import {
  getAbouts,
  getBeers,
  getBurgers,
  getSnacks
} from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores/ProductsStore.store";
import {
  GetAboutsQuery,
  GetBeersQuery,
  GetBurgersQuery,
  GetSnacksQuery
} from "../../../../__types__/apollo";
import Loading from "../../../misc/AdminLoading";
import NoData from "../../../misc/AdminNoData";
import ProductDetailWrapper from "./ProductDetailWrapper";

interface Props {
  abouts?: GetAboutsQuery;
  burgers?: GetBurgersQuery;
  snacks?: GetSnacksQuery;
  beers?: GetBeersQuery;
  productsStore?: ProductsStore;
}
interface State {
  selected: string;
}

@inject("productsStore")
@observer
export default class ProductList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      selected: null
    };
  }

  private handleAbout = () => {
    this.props.productsStore.handleAbout();
  };

  private handleBurger = () => {
    this.props.productsStore.handleBurger();
  };

  private handleSnack = () => {
    this.props.productsStore.handleSnack();
  };

  private handleBeer = () => {
    this.props.productsStore.handleBeer();
  };

  render() {
    this.props.productsStore.aboutList;
    this.props.productsStore.burgerList;
    this.props.productsStore.snackList;
    this.props.productsStore.beerList;
    return (
      <>
        {this.props.abouts ? (
          <Query<GetAboutsQuery> query={getAbouts}>
            {({ data, loading }) => {
              if (loading) return <Loading />;
              if (!data || data.abouts.length === 0) return <NoData />;
              return (
                <>
                  {this.props.productsStore.aboutList ? (
                    <div className="product-list">
                      {data.abouts.map((product, i) => (
                        <div
                          key={i}
                          id={product.id}
                          className="product"
                          onClick={e => {
                            this.props.productsStore.selectedAbout = product.id;
                            this.handleAbout();
                          }}
                        >
                          {product.title}
                        </div>
                      ))}
                    </div>
                  ) : (
                    <ProductDetailWrapper
                      aboutId={this.props.productsStore.selectedAbout}
                    />
                  )}
                </>
              );
            }}
          </Query>
        ) : null || this.props.burgers ? (
          <Query<GetBurgersQuery> query={getBurgers}>
            {({ data, loading }) => {
              if (loading) return <Loading />;
              if (!data || data.burgers.length === 0) return <NoData />;
              return (
                <>
                  {this.props.productsStore.burgerList ? (
                    <div className="product-list">
                      {data.burgers.map((product, i) => (
                        <div
                          key={i}
                          id={product.id}
                          className="product"
                          onClick={e => {
                            this.props.productsStore.selectedBurger =
                              product.id;
                            this.handleBurger();
                          }}
                        >
                          {product.title}
                        </div>
                      ))}
                    </div>
                  ) : (
                    <ProductDetailWrapper
                      burgerId={this.props.productsStore.selectedBurger}
                    />
                  )}
                </>
              );
            }}
          </Query>
        ) : null || this.props.snacks ? (
          <Query<GetSnacksQuery> query={getSnacks}>
            {({ data, loading }) => {
              if (loading) return <Loading />;
              if (!data || data.snacks.length === 0) return <NoData />;
              return (
                <>
                  {this.props.productsStore.snackList ? (
                    <div className="product-list">
                      {data.snacks.map((product, i) => (
                        <div
                          key={i}
                          id={product.id}
                          className="product"
                          onClick={e => {
                            this.props.productsStore.selectedSnack = product.id;
                            this.handleSnack();
                          }}
                        >
                          {product.title}
                        </div>
                      ))}
                    </div>
                  ) : (
                    <ProductDetailWrapper
                      snackId={this.props.productsStore.selectedSnack}
                    />
                  )}
                </>
              );
            }}
          </Query>
        ) : null || this.props.beers ? (
          <Query<GetBeersQuery> query={getBeers}>
            {({ data, loading }) => {
              if (loading) return <Loading />;
              if (!data || data.beers.length === 0) return <NoData />;
              return (
                <>
                  {this.props.productsStore.beerList ? (
                    <div className="product-list">
                      {data.beers.map((product, i) => (
                        <div
                          key={i}
                          id={product.id}
                          className="product"
                          onClick={e => {
                            this.props.productsStore.selectedBeer = product.id;
                            this.handleBeer();
                          }}
                        >
                          {product.title}
                        </div>
                      ))}
                    </div>
                  ) : (
                    <ProductDetailWrapper
                      beerId={this.props.productsStore.selectedBeer}
                    />
                  )}
                </>
              );
            }}
          </Query>
        ) : null}
      </>
    );
  }
}
