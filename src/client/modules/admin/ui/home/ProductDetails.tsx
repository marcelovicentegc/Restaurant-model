import { inject, observer } from "mobx-react";
import * as React from "react";
import Query from "react-apollo/Query";
import {
  getAbouts,
  getBeers,
  getBurgers,
  getSnacks
} from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores/ProductsStore.store";
import {
  GetAboutAbout,
  GetAboutsQuery,
  GetBeerBeer,
  GetBeersQuery,
  GetBurgerBurger,
  GetBurgersQuery,
  GetSnackSnack,
  GetSnacksQuery
} from "../../../../__types__/apollo";
import Loading from "../../../misc/Loading";
import DeleteAbout from "../delete/DeleteAbout";
import DeleteBeer from "../delete/DeleteBeer";
import DeleteBurger from "../delete/DeleteBurger";
import DeleteButton from "../delete/DeleteButton";
import DeleteSnack from "../delete/DeleteSnack";
import UpdateAbout from "../update/UpdateAbout";
import UpdateBeer from "../update/UpdateBeer";
import UpdateBurger from "../update/UpdateBurger";
import UpdateButton from "../update/UpdateButton";
import UpdateSnack from "../update/UpdateSnack";
import BackButton from "./BackButton";
import ProductList from "./ProductList";

interface Props {
  about?: GetAboutAbout;
  burger?: GetBurgerBurger;
  snack?: GetSnackSnack;
  beer?: GetBeerBeer;
  productsStore?: ProductsStore;
}

@inject("productsStore")
@observer
export default class ProductDetails extends React.Component<Props> {
  private updateAboutController = () => {
    this.props.productsStore.updateAboutController();
  };
  private deleteAboutController = () => {
    this.props.productsStore.deleteAboutController();
  };

  private updateBurgerController = () => {
    this.props.productsStore.updateBurgerController();
  };
  private deleteBurgerController = () => {
    this.props.productsStore.deleteBurgerController();
  };

  private updateSnackController = () => {
    this.props.productsStore.updateSnackController();
  };
  private deleteSnackController = () => {
    this.props.productsStore.deleteSnackController();
  };

  private updateBeerController = () => {
    this.props.productsStore.updateBeerController();
  };
  private deleteBeerController = () => {
    this.props.productsStore.deleteBeerController();
  };

  private handleAboutList = () => {
    this.props.productsStore.handleAbout();
  };

  private handleBurgerList = () => {
    this.props.productsStore.handleBurger();
  };

  private handleSnackList = () => {
    this.props.productsStore.handleSnack();
  };

  private handleBeerList = () => {
    this.props.productsStore.handleBeer();
  };

  render() {
    return this.props.about ? (
      this.props.productsStore.updateAbout ? (
        <UpdateAbout product={this.props.about} />
      ) : !this.props.productsStore.aboutList ? (
        <div className="product-detail">
          <div className="product-title">{this.props.about.title}</div>
          <div className="product-description">
            {this.props.about.description}
          </div>
          <div className="product-comp">@ {this.props.about.location}</div>
          <div className="product-pic">
            <img src={this.props.about.fileUrl} />
          </div>
          <UpdateButton updateController={this.updateAboutController} />
          <DeleteButton deleteController={this.deleteAboutController} />
          <BackButton goBack={this.handleAboutList} />
          {this.props.productsStore.deleteAbout ? (
            <DeleteAbout product={this.props.about} />
          ) : null}
        </div>
      ) : (
        <Query<GetAboutsQuery> query={getAbouts}>
          {({ loading, data }) => {
            if (loading) {
              <Loading />;
            }
            return <ProductList abouts={data} />;
          }}
        </Query>
      )
    ) : this.props.burger ? (
      this.props.productsStore.updateBurger ? (
        <UpdateBurger product={this.props.burger} />
      ) : !this.props.productsStore.burgerList ? (
        <div className="product-detail">
          <div className="product-title">{this.props.burger.title}</div>
          <div className="product-description">
            {this.props.burger.description}
          </div>
          <div className="product-comp">${this.props.burger.price}</div>
          <div className="product-pic">
            <img src={this.props.burger.fileUrl} />
          </div>
          <UpdateButton updateController={this.updateBurgerController} />
          <DeleteButton deleteController={this.deleteBurgerController} />
          <BackButton goBack={this.handleBurgerList} />
          {this.props.productsStore.deleteBurger ? (
            <DeleteBurger product={this.props.burger} />
          ) : null}
        </div>
      ) : (
        <Query<GetBurgersQuery> query={getBurgers}>
          {({ loading, data }) => {
            if (loading) {
              <Loading />;
            }
            return <ProductList burgers={data} />;
          }}
        </Query>
      )
    ) : this.props.snack ? (
      this.props.productsStore.updateSnack ? (
        <UpdateSnack product={this.props.snack} />
      ) : !this.props.productsStore.snackList ? (
        <div className="product-detail">
          <div className="product-title">{this.props.snack.title}</div>
          <div className="product-description">
            {this.props.snack.description}
          </div>
          <div className="product-comp">${this.props.snack.price}</div>
          <div className="product-pic">
            <img src={this.props.snack.fileUrl} />
          </div>
          <UpdateButton updateController={this.updateSnackController} />
          <DeleteButton deleteController={this.deleteSnackController} />
          <BackButton goBack={this.handleSnackList} />
          {this.props.productsStore.deleteSnack ? (
            <DeleteSnack product={this.props.snack} />
          ) : null}
        </div>
      ) : (
        <Query<GetSnacksQuery> query={getSnacks}>
          {({ loading, data }) => {
            if (loading) {
              <Loading />;
            }
            return <ProductList snacks={data} />;
          }}
        </Query>
      )
    ) : this.props.beer ? (
      this.props.productsStore.updateBeer ? (
        <UpdateBeer product={this.props.beer} />
      ) : !this.props.productsStore.beerList ? (
        <div className="product-detail">
          <div className="product-title">{this.props.beer.title}</div>
          <div className="product-description">
            {this.props.beer.description}
          </div>
          <div className="product-comp">${this.props.beer.price}</div>
          <div className="product-pic">
            <img src={this.props.beer.fileUrl} />
          </div>
          <UpdateButton updateController={this.updateBeerController} />
          <DeleteButton deleteController={this.deleteBeerController} />
          <BackButton goBack={this.handleBeerList} />
          {this.props.productsStore.deleteBeer ? (
            <DeleteBeer product={this.props.beer} />
          ) : null}
        </div>
      ) : (
        <Query<GetBeersQuery> query={getBeers}>
          {({ loading, data }) => {
            if (loading) {
              <Loading />;
            }
            return <ProductList beers={data} />;
          }}
        </Query>
      )
    ) : null;
  }
}
