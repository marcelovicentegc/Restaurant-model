import * as React from "react";
import Query from "react-apollo/Query";
import { getUser } from "../../../../../server/schema/graphql/Queries.graphql";
import { GetUserQuery } from "../../../../__types__/apollo";
import Loading from "../../../misc/AdminLoading";
import NoData from "../../../misc/AdminNoData";

export default class Me extends React.Component {
  render() {
    return (
      <Query<GetUserQuery> query={getUser}>
        {({ data, loading }) => {
          if (loading) {
            return <Loading />;
          }

          // 'Cannot return null for non-nullable field Query.user' GraphQL error
          if (!data) {
            return <NoData />;
          }
          return (
            <div className="admin-loading-wrapper">
              <div className="admin-no-data">Hi, {data.user.fullName} 😃</div>
            </div>
          );
        }}
      </Query>
    );
  }
}
