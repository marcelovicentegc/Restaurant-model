import * as React from "react";
import {
  GetAboutsQuery,
  GetBeersQuery,
  GetBurgersQuery,
  GetSnacksQuery
} from "../../../../__types__/apollo";
import CreateButton from "../create/CreateButton";
import Me from "./Me";
import ProductList from "./ProductList";

interface State {
  showAbouts: boolean;
  showBurgers: boolean;
  showSnacks: boolean;
  showBeers: boolean;
  showMe: boolean;
}

interface Props {
  abouts: GetAboutsQuery;
  burgers: GetBurgersQuery;
  snacks: GetSnacksQuery;
  beers: GetBeersQuery;
}

export default class Products extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      showAbouts: false,
      showBurgers: false,
      showSnacks: false,
      showBeers: false,
      showMe: true
    };

    this.handleAbouts = this.handleAbouts.bind(this);
    this.handleBeers = this.handleBeers.bind(this);
    this.handleSnacks = this.handleSnacks.bind(this);
    this.handleBurgers = this.handleBurgers.bind(this);
  }

  handleAbouts() {
    if (this.state.showAbouts) {
      this.setState({
        showAbouts: false,
        showMe: true
      });
    } else {
      this.setState({
        showAbouts: true,
        showBurgers: false,
        showSnacks: false,
        showBeers: false,
        showMe: false
      });
    }
  }

  handleBeers() {
    if (this.state.showBeers) {
      this.setState({
        showBeers: false,
        showMe: true
      });
    } else {
      this.setState({
        showBeers: true,
        showAbouts: false,
        showBurgers: false,
        showSnacks: false,
        showMe: false
      });
    }
  }

  handleSnacks() {
    if (this.state.showSnacks) {
      this.setState({
        showSnacks: false,
        showMe: true
      });
    } else {
      this.setState({
        showSnacks: true,
        showAbouts: false,
        showBurgers: false,
        showBeers: false,
        showMe: false
      });
    }
  }

  handleBurgers() {
    if (this.state.showBurgers) {
      this.setState({
        showBurgers: false,
        showMe: true
      });
    } else {
      this.setState({
        showBurgers: true,
        showAbouts: false,
        showSnacks: false,
        showBeers: false,
        showMe: false
      });
    }
  }

  render() {
    return (
      <>
        <div className="stock-wrapper">
          <div className="shelf">
            <div className="products" id="first" onClick={this.handleAbouts}>
              <div id="about" className="product">
                <h1>About</h1>
              </div>
            </div>
            <div className="products" onClick={this.handleBurgers}>
              <div id="burgers" className="product">
                <h1>Burgers</h1>
              </div>
            </div>
            <div className="products" onClick={this.handleSnacks}>
              <div id="snacks" className="product">
                <h1>Snacks</h1>
              </div>
            </div>
            <div className="products" onClick={this.handleBeers}>
              <div id="beers" className="product">
                <h1>Beers</h1>
              </div>
            </div>
          </div>
          <CreateButton />
          {this.state.showMe ? <Me /> : null}
          {this.state.showAbouts ? (
            <ProductList abouts={this.props.abouts} />
          ) : null || this.state.showBurgers ? (
            <ProductList burgers={this.props.burgers} />
          ) : null || this.state.showSnacks ? (
            <ProductList snacks={this.props.snacks} />
          ) : null || this.state.showBeers ? (
            <ProductList beers={this.props.beers} />
          ) : null}
        </div>
      </>
    );
  }
}
