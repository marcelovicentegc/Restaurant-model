import { inject, observer } from "mobx-react";
import * as React from "react";
import Query from "react-apollo/Query";
import {
  getAbout,
  getBeer,
  getBurger,
  getSnack
} from "../../../../../server/schema/graphql/Queries.graphql";
import { ProductsStore } from "../../../../stores";
import {
  GetAboutQuery,
  GetAboutsQuery,
  GetAboutVariables,
  GetBeerQuery,
  GetBeersQuery,
  GetBeerVariables,
  GetBurgerQuery,
  GetBurgersQuery,
  GetBurgerVariables,
  GetSnackQuery,
  GetSnacksQuery,
  GetSnackVariables
} from "../../../../__types__/apollo";
import Loading from "../../../misc/AdminLoading";
import NoData from "../../../misc/AdminNoData";
import ProductDetails from "./ProductDetails";

interface Props {
  aboutId?: string;
  burgerId?: string;
  snackId?: string;
  beerId?: string;
  productsStore?: ProductsStore;
  product?: string;
}

interface State {
  abouts?: GetAboutsQuery;
  burgers?: GetBurgersQuery;
  snacks?: GetSnacksQuery;
  beers?: GetBeersQuery;
}

@inject("productsStore")
@observer
export default class ProductDetailWrapper extends React.Component<
  Props,
  State
> {
  render() {
    return this.props.burgerId ? (
      <Query<GetBurgerQuery, GetBurgerVariables>
        query={getBurger}
        variables={{ id: this.props.productsStore.selectedBurger }}
      >
        {({ data, loading }) => {
          if (loading) return <Loading />;
          if (!data) return <NoData />;
          return <ProductDetails burger={data.burger} />;
        }}
      </Query>
    ) : null || this.props.aboutId ? (
      <Query<GetAboutQuery, GetAboutVariables>
        query={getAbout}
        variables={{ id: this.props.aboutId }}
      >
        {({ data, loading }) => {
          if (loading) return <Loading />;
          if (!data) return <NoData />;
          return <ProductDetails about={data.about} />;
        }}
      </Query>
    ) : null || this.props.snackId ? (
      <Query<GetSnackQuery, GetSnackVariables>
        query={getSnack}
        variables={{ id: this.props.productsStore.selectedSnack }}
      >
        {({ data, loading }) => {
          if (loading) return <Loading />;
          if (!data) return <NoData />;
          return <ProductDetails snack={data.snack} />;
        }}
      </Query>
    ) : null || this.props.beerId ? (
      <Query<GetBeerQuery, GetBeerVariables>
        query={getBeer}
        variables={{ id: this.props.beerId }}
      >
        {({ data, loading }) => {
          if (loading) return <Loading />;
          if (!data) return <NoData />;
          return <ProductDetails beer={data.beer} />;
        }}
      </Query>
    ) : null;
  }
}
