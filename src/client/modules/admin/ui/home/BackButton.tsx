import * as React from "react";

interface Props {
  goBack(): void;
  atUpdate?: string;
}

const BackButton = (props: Props) => {
  return (
    <>
      {props.atUpdate ? (
        <button className={props.atUpdate} onClick={props.goBack}>
          <span>Edit</span>
        </button>
      ) : (
        <button className="back-button" onClick={props.goBack}>
          <span>Edit</span>
        </button>
      )}
    </>
  );
};

export default BackButton;
