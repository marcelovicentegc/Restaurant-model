import * as React from "react";
import { Link } from "react-router-dom";
import Logout from "./Logout";

export default class NavBar extends React.Component {
  render() {
    return (
      <>
        <nav>
          <div className="title">
            <p>
              <span />
            </p>
          </div>
          <div className="nav-items">
            <p className="nav-item">
              <Link to={"/"}>Home</Link>
            </p>
            <Logout />
          </div>
        </nav>
      </>
    );
  }
}
