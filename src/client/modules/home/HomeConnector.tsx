import * as React from "react";
import HomeView from "./ui/HomeView";

const HomeConnector = () => <HomeView />;
export default HomeConnector;
