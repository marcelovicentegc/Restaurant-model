import * as React from "react";
import { hot } from "react-hot-loader";
import SmoothScroll from "../../misc/SmoothScroll";
import Footer from "./footer/Footer";
import "./main.scss";
import MenuWrapper from "./menu/MenuWrapper";
import About from "./products/About";
import Beers from "./products/Beers";
import Burgers from "./products/Burgers";
import Snacks from "./products/Snacks";

const HomeConnector = () => {
  return (
    <React.Fragment>
      <div id="menu-root">
        <MenuWrapper />
      </div>
      <div id="showcase-root">
        <About />
        <Burgers />
        <Snacks />
        <Beers />
      </div>
      <div id="footer-root">
        <Footer />
      </div>
      <SmoothScroll />
    </React.Fragment>
  );
};

export default hot(module)(HomeConnector);
