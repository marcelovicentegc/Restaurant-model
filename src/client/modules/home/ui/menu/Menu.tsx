import * as React from "react";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";

interface Props {
  menuVisibility: any;
  handleMouseDown: any;
  navButtonHandleMouseDown: any;
}

class Menu extends React.Component<RouteComponentProps<{}> & Props> {
  render() {
    var visibility = "hide";

    if (this.props.menuVisibility) {
      visibility = "show";
    }

    return (
      <div id="flyoutMenu" className={visibility}>
        <button
          id="close-menu-button"
          onMouseDown={this.props.handleMouseDown}
        />
        <ul>
          <li>
            <a
              href="#burgers"
              className="nav-button"
              onMouseDown={this.props.navButtonHandleMouseDown}
            >
              BURGERS
            </a>
          </li>
          <li>
            <a
              href="#snacks"
              className="nav-button"
              onMouseDown={this.props.navButtonHandleMouseDown}
            >
              SNACKS
            </a>
          </li>
          <li>
            <a
              href="#beers"
              className="nav-button"
              onMouseDown={this.props.navButtonHandleMouseDown}
            >
              BEERS
            </a>
          </li>
          <li className="staff">
            <Link to="/admin" onMouseDown={this.props.navButtonHandleMouseDown}>
              Staff
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default withRouter(Menu);
