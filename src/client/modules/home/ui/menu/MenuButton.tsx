import * as React from "react";

interface Props {
  handleMouseDown: any;
}

class MenuButton extends React.Component<Props> {
  render() {
    return (
      <nav>
        <button id="menu-button" onMouseDown={this.props.handleMouseDown} />
        <p>
          <a href="#">THE RESTAURANT</a>
        </p>
      </nav>
    );
  }
}

export default MenuButton;
