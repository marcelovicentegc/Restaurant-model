import * as React from "react";
import "./main.scss";
import Menu from "./Menu";
import MenuButton from "./MenuButton";

interface Props {}

interface State {
  visible: boolean;
}

class MenuWrapper extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      visible: false
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.navButtonHandleMouseDown = this.navButtonHandleMouseDown.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState({
      visible: !this.state.visible
    });
  }

  handleMouseDown(e: Event) {
    this.toggleMenu();
    e.stopPropagation();
  }

  navButtonHandleMouseDown(e: Event) {
    setTimeout(() => {
      this.toggleMenu();
    }, 200);
  }

  render() {
    return (
      <div id="menu-wrapper">
        <MenuButton handleMouseDown={this.handleMouseDown} />
        <Menu
          handleMouseDown={this.handleMouseDown}
          navButtonHandleMouseDown={this.navButtonHandleMouseDown}
          menuVisibility={this.state.visible}
        />
      </div>
    );
  }
}

export default MenuWrapper;
