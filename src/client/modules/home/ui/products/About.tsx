import * as React from "react";
import Query from "react-apollo/Query";
import * as OwlCarousel from "react-owl-carousel";
import "../../../../../../node_modules/owl.carousel/dist/assets/owl.carousel.css";
import "../../../../../../node_modules/owl.carousel/dist/assets/owl.theme.default.css";
import { getAbouts } from "../../../../../server/schema/graphql/Queries.graphql";
import { GetAboutsQuery } from "../../../../__types__/apollo";
import Loading from "../../../misc/ShowcaseLoading";
import NoData from "../../../misc/ShowcaseNoData";

export default class About extends React.Component {
  render() {
    return (
      <div id="about" className="showcase">
        <div id="about-title" className="spa-title">
          <p>About</p>
        </div>
        <div id="about-content">
          <Query<GetAboutsQuery> query={getAbouts}>
            {({ loading, data, error }) => {
              if (loading) return <Loading />;
              if (!data || data.abouts.length === 0) {
                return <NoData />;
              }
              if (error)
                return (
                  <div className="carousel-cell">
                    <h1>{error}</h1>
                  </div>
                );
              return (
                <>
                  <OwlCarousel.default
                    className="spa-content"
                    margin={10}
                    items={1}
                  >
                    {data.abouts.map((product, i) => (
                      <div className="carousel-cell" key={i}>
                        {product.fileUrl ? (
                          <div className="product-pic">
                            <img src={product.fileUrl} />
                          </div>
                        ) : (
                          <div className="product-pic">
                            <img src="https://placekitten.com/g/2000/1000" />
                          </div>
                        )}
                        <div className="product-description">
                          <p className="product-title">{product.title}</p>
                          <p className="product-details">
                            {product.description}
                          </p>
                          <p className="product-price">@{product.location}</p>
                        </div>
                      </div>
                    ))}
                  </OwlCarousel.default>
                </>
              );
            }}
          </Query>
        </div>
      </div>
    );
  }
}
