import * as React from "react";
import Query from "react-apollo/Query";
import * as OwlCarousel from "react-owl-carousel";
import "../../../../../../node_modules/owl.carousel/dist/assets/owl.carousel.css";
import "../../../../../../node_modules/owl.carousel/dist/assets/owl.theme.default.css";
import { getBeers } from "../../../../../server/schema/graphql/Queries.graphql";
import { GetBeersQuery } from "../../../../__types__/apollo";
import ShowcaseLoading from "../../../misc/ShowcaseLoading";
import NoData from "../../../misc/ShowcaseNoData";

export default class Beers extends React.Component {
  render() {
    return (
      <div id="beers" className="showcase">
        <div id="beers-title" className="spa-title">
          <p>Beers</p>
        </div>
        <div id="beers-content">
          <Query<GetBeersQuery> query={getBeers}>
            {({ loading, data, error }) => {
              if (loading) return <ShowcaseLoading />;
              if (!data || data.beers.length === 0) {
                return <NoData />;
              }
              if (error)
                return (
                  <div className="carousel-cell">
                    <h1>{error}</h1>
                  </div>
                );
              return (
                <>
                  <OwlCarousel.default
                    className="spa-content"
                    margin={10}
                    items={1}
                  >
                    {data.beers.map((product, i) => (
                      <div className="carousel-cell" key={i}>
                        {product.fileUrl ? (
                          <div className="product-pic">
                            <img src={product.fileUrl} />
                          </div>
                        ) : (
                          <div className="product-pic">
                            <img src="https://placekitten.com/g/2000/1000" />
                          </div>
                        )}
                        <div className="product-description">
                          <p className="product-title">{product.title}</p>
                          <p className="product-details">
                            {product.description}
                          </p>
                          <p className="product-price">${product.price}</p>
                        </div>
                      </div>
                    ))}
                  </OwlCarousel.default>
                </>
              );
            }}
          </Query>
        </div>
      </div>
    );
  }
}
