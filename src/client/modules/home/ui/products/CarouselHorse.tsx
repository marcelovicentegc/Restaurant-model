import * as React from "react";
import {
  GetAboutsAbouts,
  GetBeersBeers,
  GetBurgersBurgers,
  GetSnacksSnacks
} from "../../../../__types__/apollo";

interface Product {
  id: string;
  title: string;
  description: string;
  price?: number;
  location?: string;
  upload?: File;
}

interface Props {
  product: Product;
}

export default class CarouselHorse extends React.Component<Props> {
  picOrNoPic() {
    if (this.props.product.upload) {
      return (
        <div className="product-pic">
          {/* <img src={this.props.product.upload.filename}></img> */}
          <p>Success!!</p>
        </div>
      );
    }
    if (this.props) {
      console.log(this.props);
      return (
        <div className="product-pic">
          <img src="https://placekitten.com/g/2000/1000" />
        </div>
      );
    }
  }

  payOrGo() {
    if (this.props.product.price) {
      return "$" + this.props.product.price;
    }
    if (this.props.product.location) {
      return this.props.product.location;
    }
  }

  render() {
    return (
      <div className="carousel-cell">
        {this.picOrNoPic()}
        <div className="product-description">
          <p className="product-title">{this.props.product.title}</p>
          <p className="product-details">{this.props.product.description}</p>
          <p className="product-price">{this.payOrGo()}</p>
        </div>
      </div>
    );
  }
}
