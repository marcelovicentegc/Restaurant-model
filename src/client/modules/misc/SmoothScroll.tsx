import * as React from 'react';

export default class SmoothScroll extends React.Component {
    constructor(props:any) {
        super(props)
    }

    instance:any;

    componentDidMount() {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.innerHTML = `document.querySelectorAll('a[href^="#"]').forEach(anchor => {anchor.addEventListener('click', function (e) {e.preventDefault(); document.querySelector(this.getAttribute('href')).scrollIntoView({behavior: 'smooth'});});})`;
        this.instance.appendChild(script);
    }

    render() {
        return <div ref={el => (this.instance = el)} />;
    }
}