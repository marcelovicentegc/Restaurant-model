import * as React from "react";

const Loading = () => {
  return (
    <div className="showcase-loading-wrapper">
      <div className="lds-grid">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};

export default Loading;
