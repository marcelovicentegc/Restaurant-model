import * as React from "react";

const NoData = () => {
  return (
    <div className="showcase-loading-wrapper">
      <div className="showcase-no-data">No data 😮!</div>
    </div>
  );
};

export default NoData;
