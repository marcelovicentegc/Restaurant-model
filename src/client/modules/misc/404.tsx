import * as React from "react";

const FourOhFour = () => {
  return (
    <div>
      <p>Page not found :(</p>
    </div>
  );
};

export default FourOhFour;
