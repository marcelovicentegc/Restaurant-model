import * as React from "react";

const NoData = () => {
  return (
    <div className="admin-loading-wrapper">
      <div className="admin-no-data">No data 😮!</div>
    </div>
  );
};

export default NoData;
